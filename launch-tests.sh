#!/bin/sh
./chromedriver --whitelisted-ips --port=4444 &
DRIVER_PID=$!
# nohup sh -c /app/chromedriver --whitelisted-ips &

flutter drive --target=test_driver/app.dart --release

kill $DRIVER_PID
