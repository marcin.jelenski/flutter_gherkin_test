Feature: Submitting a form.
  The text form.

  Background:
    Given I am on the counter screen

  Scenario: Dialog shows after form submit
    When I tap the "input" field
    And I type "abc" text
    And I tap the "submit" button
    Then I expect to see "Typed text: abc" text

  Scenario: Dialog shows text after increment
    When I tap the "input" field
    And I type "lorem ipsum" text
    And I tap the "increment" button
    And I wait 2 seconds
    And I tap the "submit" button
    Then I expect to see "Typed text: lorem ipsum" text