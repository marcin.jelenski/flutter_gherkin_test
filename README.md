# gherkin_test

Gherkin tests

## Simulator tests

1. Launch iOS/Android simulator.
2. Run flutter drive

```bash
flutter drive --target=test_driver/app.dart
```

## Web tests

Chrome Driver:

```
./chromedriver --port=4444
```

Tests:

```bash
# headless
flutter drive --target=test_driver/app.dart --browser-name=chrome -d web-server --web-port=8080

# on-screen
flutter drive --target=test_driver/app.dart -d web-server --web-port=8080 --no-headless
```

Reference: https://github.com/flutter/flutter/wiki/Running-Flutter-Driver-tests-with-Web
